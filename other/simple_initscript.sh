#!/bin/sh
#
# SIMPLE INIT SCRIPT FOR DAEMONIZING CAKE CONSOLE SCRIPTS (while(true) ...)
# Jasper Smet
#

CAKE_CONSOLE_SCRIPT=/path/to/cake #cake's executable console script
CAKE_WORKING_DIR=/path/to/my/app/dir
USERNAME=www

# TVDB SPECIFIC

TVDB_PIDFILE=/var/run/my_pid.pid
TVDB_COMMAND="myshellscript arg"

start() {
	echo "Starting Daemon"
	start-stop-daemon --start --pidfile $TVDB_PIDFILE --make-pidfile --chuid $USERNAME --exec $CAKE_CONSOLE_SCRIPT --background -- -working $CAKE_WORKING_DIR $TVDB_COMMAND
}

stop() {
	echo "Stopping Daemon"
	start-stop-daemon --stop --quiet --pidfile $TVDB_PIDFILE
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	*)
	echo "Usage: {start|stop|restart}"
	exit 1
esac

exit 0