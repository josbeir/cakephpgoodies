A simple collection a cake goodies
==================================


View/Helpers/BlockHelper 
-------------------
A simple but very useful helper ment for loading elements in regions from views and output them in your layout file from a predefined variable (bit like the drupal region system).

####Usage
Place one of the following examples in your view, the output will be available in your layout file.
Be aware that all block elements need to be placed in a subfolder of /View/Elements followed by the name of the region and the element name: /View/Elements/right_region/myelement.ctp

	// simple single example
	$this->Block->add('element');
	
	// simple single example with options	
	$this->Block->add('element', $options);

	// simple multiple element example
	$this->Block->add(array(
		'myElement1',
		'myElement2'
	));
	
	// multiple element example with some options	
	$this->Block->add(array(
		'myElement' => array('region' => 'top_region', 'var1' => $var1),
		'myElement2' => array('region' => 'left_region', 'var1' => $var1)
	));
	
In your layout.ctp file use something like this to output the blocks you added in your view

	<?php if($this->Block->check('region_right)) : ?>
		<div id="region-right">
			<?php echo $region_right; ?>
		</div
	<?php endif; ?>
	

Model/Behavior/ExtendedCounterCacheBehavior
-------------------

See the file's head for it's documenation

other/simple_initscript.sh	
----------------------------------
* A very basic init script for daemonizing cake console daemons and attaching a pid file to them so they 
  can be monitored with Monit or some other process monitoring tool