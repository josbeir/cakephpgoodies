<?php
/**
 * Block helper using page regions for CakePHP 2.x
 *
 * USAGE:
 *   The default region is region_right and is availabe in the layout files as a
 *   variable.
 *   Other regions can be defined as an option passed to the block in Block::add : array('region' => '<regionName>').
 *
 *   Some usage examples for a view:
 *   $block->add('blockname', $options); 
 *   $block->add('blockname', array('region' => 'top'));
 *   $block->add('blockname', array('region' => 'top', 'autopath' => false));
 *   $block->add(array(
 *     'blockname0',
 *     'blockname1' => $options,
 *     'blockname2' => $options,
 *   ));
 *   
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Jasper Smet < jasper AT followmy.tv >
 * @link          https://github.com/josbeir/cakephp_scripts
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class BlockHelper extends AppHelper {

	/**
	 *  View object for this instance
	 *
	 * @var object
	 */
	private $View;
	
	/**
	 * The default region thats used for blocks
	 *
	 * @var string
	 */
	private $defaultRegion = 'region_right';
	
	/**
	 * All blocks elements that are set for current instance
	 *
	 * @var array
	 */
	private $elements = array();
	
	/**
	 * Set the View object for this instance
	 *
	 * @param View $View The View this helper is being attached to.
	 */
	public function __construct(View $View, $settings = array()) {
		parent::__construct($View);
		
		if(!empty($settings['defaultRegion'])) {
			$this->defaultRegion = $settings['defaultRegion'];
		}
	}

	/**
	 * Make the block region variables available in the view
	 */
	public function afterRender() {
		if(!empty($this->elements)) {
			foreach($this->elements as $region => $elements) {
				$this->_View->set($region, $this->__getElements($elements));
			}
		}
	}
	
	/**
	 * Add a new block to be used in regions
	 *
	 * @param mixed $block Can be eithen an array with key as blockname and and optionally a value array as options
	 * @param array $options Options that are passed to the block element, use $options['region'] to define a region
	 */
	public function add($block = null, $options = array()) {
		if(is_string($block)) {
			$this->_setElement($block, $options);
		} else if(is_array($block)) {
			foreach($block as $block_key => $block_val) {
				if(is_numeric($block_key) && is_string($block_val)) {
					$this->_setElement($block_val);
				} elseif(is_string($block_key) && is_array($block_val)) {
					$this->_setElement($block_key, $block_val);
				} else {
					$this->log('Malformed block arguments', LOG_DEBUG);
				}
			}
		}
	}

	/**
	 * Check if a region is filled
	 *
	 * @param string $region name of region
	 * @return boolean
	 */
	public function check($region = null) {
		$region = (!empty($region) ? $region : $this->defaultRegion);
		
		$return = false;
		if(!empty($this->elements[$region])) {
			$elements = trim(implode($this->elements[$region]));
			$return = (!empty($elements));
		}
		
		return $return;
	}
	
	/**
	 * Combine all block content and return it
	 * 
	 * @param array $elements block element content
	 * @return string output of all blocks
	 */
	private function __getElements($elements) {
		return $this->output(implode($elements));
	}
	
	/**
	 * Add an element to the block collection
	 * @param string $name name of the block element
	 * @param array $options options to pass to the element
	 */
	private function _setElement($name, $options = array()) {
		
		$region = $this->defaultRegion;
		if(!empty($options['region'])) {
			$region = $options['region'];
		}
		
		if(!isset($options['autopath']) || $options['autopath'] === true) {
			$path = 'blocks/'. $region .'/'.$name;
		} else {
			$path = $name;
		}

		$this->elements[$region][] = $this->_View->element($path, $options);
	}
}
?>