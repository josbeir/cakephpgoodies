<?php
/**
 * ExtendedCounterCacheBehavior, take control of your counter caches!
 * CakePHP 2.x
 *
 * This behavior uses cake's existing counter cache functionality but basically gives you more flexibility 
 * of what get's counterCached by using replacement patterns from a main dataset in your scope
 * 
 * Some use cases
 * - counter caches for polymorphic models (see example)
 * - counter caches based of a specific records data
 * - counter cache based on date range - don't forget to write a cron or something to check them regualry tho :-)
 *
 * Basically everything in the scope can be set as a replacement pattern, for a polymorphic model you can do something like this:
 * 
 * In this example i have a field called 'model' and 'foreign_key' in my main dataset.
 *
 * 	public $actsAs = array(
 *		'ExtendedCounterCache' => array(
 * 			':model' => array(
 *				'belongsToOptions' => array(), // default belongsTo options (optional, handy for polymorphic models, default options will be merged)
 *				'defaultScope' => array(
 *					'News.foreign_key' => ':foreign_key',
 *					'News.model' => ':model',
 *					'News.start_date <=' => ':_datetime',
 *					'News.end_date >=' => ':_datetime'
 *				),
 *				'counterFields' => array(
 *					'news_count', // standard 'total' counter cache field
 *					'news_blog_count' => array(
 *						'News.published' => 1,
 *						'News.type' => 'blog',
 *					)
 *				)
 *			)
 *		)
 *	);
 *
 * Currently there are two 'internal' replacement patterns: :_datetime and :_date
 * 
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Jasper Smet < jasper AT followmy.tv >
 * @link          https://github.com/josbeir/CakePHPGoodies
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
*/
class ExtendedCounterCacheBehavior extends ModelBehavior {
	
	public $settings = array();
	
	public function setup(Model $Model, $settings = array()) {
		$this->settings[$Model->alias] = $settings;
	}
	
	public function beforeSave(Model $Model) {
		$this->attach($Model);
		return true;
	}
	
	public function beforeDelete(Model $Model) {
		$this->attach($Model);
		return true;
	}
	
	/** 
	 * Attach/merge the belongsTo counterCache fields + scope
	 */
	private function attach(Model $Model) {
		$data = $this->getData($Model);

		foreach($this->settings[$Model->alias] as $model_name => $settings) {
			$counterCache = array();
			$defaultScope = array();
			$belongsToOptions = array();
			
			if(!is_array($settings)) {
				$model_name = $this->replacementPatterns($settings, $data);
				$field = strtolower(Inflector::underscore($model_name)) .'_count';
				$counterCache[$field] = $defaultScope;
			} else {
				$model_name = $this->replacementPatterns($model_name, $data);
				if(!empty($settings['defaultScope'])) {
					$defaultScope = $this->replacementPatterns($settings['defaultScope'], $data);
				}
				if(!empty($settings['counterFields'])) {
					foreach($settings['counterFields'] as $field => $scope) {
						if(is_array($scope)) {
							$counterCache[$field] = $defaultScope + $this->replacementPatterns($scope, $data);
						} else {
							$counterCache[$scope] = $defaultScope;
						}
					}
				}
				if(!empty($settings['belongsToOptions'])) {
					$belongsToOptions = $this->replacementPatterns($settings['belongsToOptions'], $data);
				}
			}

			$belongsTo = array(
				$model_name => $belongsToOptions + array(
					'counterCache' => $counterCache,
					'className' => $model_name
				)
			);
			
			// merge belongsTo settings with the model's one
			$Model->belongsTo = Set::pushDiff($Model->belongsTo, $belongsTo);
		}
	}
	
	/** 
	 * Replace everything in $item using $data as reference
	 */ 
	private function replacementPatterns($item, $data) {
		if(is_string($item)) {
			return String::insert($item, $data);
		}
		if(is_array($item)) {
			$parsed = array();
			foreach($item as $key => $value) {
				$key = String::insert($key, $data);
				$value = String::insert($value, $data);
				$parsed[$key] = $value;
			}
			return $parsed;
		}
		return false;
	}
	
	/**
	 * Returns data from the main record
	 * If $Model->data is empty a record will be loaded 
	 * if the $Model->id is set
	 */ 
	private function getData(Model $Model) {
		$data = array();
		if(!empty($Model->data[$Model->alias])) {
			$data = $Model->data[$Model->alias];
		} else if(!empty($Model->id)) {
			$Model->recursive = -1;
			$record = $Model->read();
			if(!empty($record[$Model->alias])) {
				$data = $record[$Model->alias];
			} 
		}
		
		// extra handy replacement patterns
		if(!empty($data)) {
			$data = $data + array(
				'_date' => date('Y-m-d'),
				'_datetime' => date('Y-m-d H:i:s')
			);
		}
		return $data;
	}
	
}

?>